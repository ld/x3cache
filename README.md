# X3Cache (J2Cache 3.x) 项目规划 

J2Cache 本身作为 Java 的两级缓存框架的构想到 2.3.19 版本已经基本实现完毕，接下来 2.x 这个分支只做 Bug 的修复，不再进一步开发。

从 2018年5月23日 其开始计划 3.x 版本，3.x 一个整体的目标是实现对跨语言的支持。在一个混合语言开发系统中可以实现缓存数据的共享和更新通知。

3.0 版本代号 —— X3Cache

提供不同语言的实现：

* [J2Cache](https://gitee.com/ld/J2Cache)
* PHP2Cache
* [Py3Cache](https://gitee.com/ld/Py3Cache)
* JS2Cache
* Cpp2Cache
* Go2Cache
* ......

### 项目计划

1. 规范制定，包括数据序列化规范以及节点通知数据规范 （7月31日）
2. 规范的 Java 参考实现 —— J2Cache 3.0
3. 征集各个语言实现版本的开发负责人（如果有意请加 QQ 7118343）
4. 计划2018年底实现主流语言的版本
